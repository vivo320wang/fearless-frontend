import React from "react";

class ConferenceForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            // maxPresentations: '',
            // maxAttendees: '',
            max_presentations: '',
            max_attendees: '',
            location: '',
            locations: [],
        };

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleEndDateChange = this.handleEndDateChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handlePresentationChange = this.handlePresentationChange.bind(this);
        this.handleAttendeeChange = this.handleAttendeeChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    async componentDidMount(){
        const url = "http://localhost:8000/api/locations/"
        
        const response = await fetch(url);
    
        if(response.ok){
            const data = await response.json();
            this.setState({locations: data.locations})
    
        
            // const locationSelectTag = document.querySelector('#location');
            //     for (let location of data.locations){
            //         const option = document.createElement('option')
            //         option.value = location.id 
            //         option.innerHTML = location.name
            //         locationSelectTag.appendChild(option)
            //         // console.log(location)
            //     }
    
            }
    }

    async handleSubmit(event){
        event.preventDefault();
        const data = {...this.state};
        // data.max_presentation = data.maxPresentations;
        // delete data.maxPresentations;
        delete data.locations;
        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);
          this.setState({
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            location: '',
          });
        }
      }
            // const cleared = {
            //     name: '',
            //     starts: '',
            //     ends: '',
            //     description: '',
            //     // maxPresentations: '',
            //     // maxAttendees: '',
            //     max_presentations: '',
            //     max_attendees: '',
            //     location: '',
            // };
            // this.setState(cleared);
  

    handleNameChange(event){
        const value = event.target.value;
        this.setState({name:value})
    }

    handleStartDateChange(event){
        const value = event.target.value;
        this.setState({starts:value})
    }

    handleEndDateChange(event){
        const value = event.target.value;
        this.setState({ends:value})
    }

    handleDescriptionChange(event){
        const value = event.target.value;
        this.setState({description:value})
    }

    handlePresentationChange(event){
        const value = event.target.value;
        this.setState({max_presentations:value})
    }

    handleAttendeeChange(event){
        const value = event.target.value;
        this.setState({max_attendees:value})
    } 


    handleLocationChange(event){
        const value = event.target.value;
        this.setState({location:value})
        
    } 


    render(){
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="Name" required type="text" id="name" className="form-control" name="name" value={this.state.name}/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleStartDateChange} placeholder="Starts" required type="date" id="starts" className="form-control" name="starts" value={this.state.start} />
                    <label htmlFor="starts">Starts</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEndDateChange} placeholder="Ends" required type="date" id="ends" className="form-control" name="ends" value={this.state.end}/>
                    <label htmlFor="ends">Ends</label>
                  </div>
                  <div className="mb-3">
                    <label htmlFor="description" className="form-label">Description</label>
                    <textarea onChange={this.handleDescriptionChange} className="form-control" id="description" rows="3" name="description" value={this.state.description}></textarea>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handlePresentationChange} placeholder="Maximum_presentations" required type="number" id="max_presentations" className="form-control" name="max_presentations" value={this.state.maxPresentation}/>
                    <label htmlFor="maximum_presentations">Maximum presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleAttendeeChange} placeholder="Maximum-attendees" required type="number" id="max_attendees" className="form-control" name="max_attendees" value={this.state.maxAttendee}/>
                    <label htmlFor="maximum-attendees">Maximum attendees</label>
                  </div>
                  <div className="mb-3">
                    <select onChange={this.handleLocationChange} required id="location" className="form-select" name="location" value={this.state.location}>
                      <option value="">Choose a location</option>
                      {this.state.locations.map(location=>{
                          return (
                              <option key={location.id} value={location.id}>
                              {location.name}
                              </option>
                              );
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
    }
}
export default ConferenceForm