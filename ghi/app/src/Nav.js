import {NavLink} from 'react-router-dom';

function Nav () {
    return (
    <nav className ="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
            <NavLink className="navbar-brand" to="#">Conference GO!</NavLink>
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
                     </li>
                    <li>
                        <NavLink to ="/locations/new" className="nav-link " aria-current="page"  id="location_nav">New location</NavLink>
                    </li>
                    <li>
                        <NavLink to ="/conferences/new" className="nav-link" aria-current="page" id="conference_nav">New conference</NavLink>
                    </li>
                    <li>
                        <NavLink to ="/attendees/new" className="nav-link" aria-current="page" id="conference_nav">New attendee</NavLink>    
                    </li>
                    <li>
                        <NavLink to ="/presentations/new" className="nav-link" aria-current="page" id="conference_nav">New presentation</NavLink>    
                    </li>
                </ul>
        </div>    
    </nav> 
    );
}

export default Nav;